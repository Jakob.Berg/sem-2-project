package no.uib.inf101.wordle.controller;

import no.uib.inf101.wordle.model.GameState;
import no.uib.inf101.wordle.view.ViewableWordleModel;

public interface ControllableWordleModel extends ViewableWordleModel {
      
  /**
  * Checks if the position of a Tetromino has changed.
  *
  * @param deltaRow The change in the rows
  * @param deltaCol The change in the cols
  * @return boolean that checks if the position has changed
  */
  public boolean moveBox(int deltaRow, int deltaCol);
  
  
  /** Sets the GameState to MAIN_MENU at first. Then if the GameState is changed, it returns the new gameChange.
  * @return Returns the state of the game
  */
  public GameState getGameState();
  
}
