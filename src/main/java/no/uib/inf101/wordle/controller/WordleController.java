package no.uib.inf101.wordle.controller;

import javax.swing.Timer;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import no.uib.inf101.wordle.model.GameState;
import no.uib.inf101.wordle.model.WordleModel;
import no.uib.inf101.wordle.view.WordleView;

public class WordleController implements java.awt.event.KeyListener {
    private ControllableWordleModel controllableWordleModel;
    private WordleView wordleView;


  /**
 * Constructs a new TetrisController.
 * Initializes the game model, view, timer for game logic updates, and starts playing the Tetris theme song.
 * The controller also registers itself as a key listener to the view to handle user input.
 *
 * @param controllableTetrisModel The game model that this controller will manage.
 * @param tetrisView The game view that this controller will update based on game state changes.
 */
  public WordleController(ControllableWordleModel controllableWordleModel, WordleView wordleView) {
    this.controllableWordleModel = controllableWordleModel;
    this.wordleView = wordleView;
    wordleView.addKeyListener(this);
    wordleView.setFocusable(true);
  }

  @Override
  public void keyTyped(KeyEvent e) {
    throw new UnsupportedOperationException("Unimplemented method 'keyTyped'");
  }

  @Override
  public void keyPressed(KeyEvent e) {
    if (e.getKeyCode() == KeyEvent.VK_LEFT) {
      controllableWordleModel.moveBox(0, -1);
      // Left arrow was pressed

    } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
      controllableWordleModel.moveBox(0, 1);
      // Right arrow was pressed

    } else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
      controllableWordleModel.moveBox(0, 0);
      // Up arrow was pressed

    } else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
      if (controllableWordleModel.getGameState() == GameState.ACTIVE_GAME) {
        //controllableWordleModel.getNext();

      }
      if (controllableWordleModel.getGameState() == GameState.MAIN_MENU) {
        ((WordleModel) controllableWordleModel).startGame();
      }
      // Spacebar was pressed

    } else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
      if (controllableWordleModel.getGameState() == GameState.GAME_OVER) {
        ((WordleModel) controllableWordleModel).mainMenu();
      }
      // Escape was pressed

    }
    
    wordleView.repaint();

  }

  @Override
  public void keyReleased(KeyEvent e) {
    throw new UnsupportedOperationException("Unimplemented method 'keyReleased'");
  }
}
