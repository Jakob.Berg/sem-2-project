package no.uib.inf101.wordle.model;

public enum GameState {
    ACTIVE_GAME,
    GAME_OVER,
    MAIN_MENU
}
