package no.uib.inf101.wordle.model;

import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.Grid;

public class WordleBoard extends Grid<Character> {
  private int rows;
  private int cols;

  public WordleBoard(int rows, int cols) {
    // Initializes a new Tetris board with the specified number of rows and columns, 
    // filling the grid with '-' to indicate empty cells.
    super(rows, cols, '-');
    this.rows = rows;
    this.cols = cols;

  }

  //Checks if a specific row is fully occupied (no empty cells).
  public boolean isRowFull(int row) {
    for (int col = 0; col < cols; col++) {
      if (get(new CellPosition(row, col)) == '-') {
        return false;
      }

    }
    return true;
  }
  
  

}
