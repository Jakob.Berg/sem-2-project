package no.uib.inf101.wordle.model;

import java.util.Iterator;

import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.Grid;
import no.uib.inf101.grid.GridCell;
import no.uib.inf101.grid.GridDimension;
import no.uib.inf101.wordle.controller.ControllableWordleModel;
import no.uib.inf101.wordle.model.word.Word;
import no.uib.inf101.wordle.model.word.RandomWordFactory;
import no.uib.inf101.wordle.model.word.WordFactory;
import no.uib.inf101.wordle.view.WordleView;

public class WordleModel implements ControllableWordleModel{
  private WordleBoard wordleBoard;
    private WordFactory wordFactory;
    private String currentWord;
    private GameState gameState;
    private WordleView view;
    private int currentAttempt;
    private String guessedWord;
  
  /**
 * Constructs a new TetrisModel with a specified Tetris board and Tetromino factory.
 * Initializes the game state to MAIN_MENU, fetches the next Tetromino from the factory,
 * and positions it at the top center of the board.
 *
 * @param wordleBoard The Tetris board to be used by this model. 
 * @param wordleFactory The factory for creating Tetromino pieces. 
 */
  public WordleModel(WordleBoard wordleBoard, WordFactory wordFactory){
    this.wordleBoard = wordleBoard;
    this.wordFactory = wordFactory;
    this.currentWord = this.wordFactory.createWord().getWord();
    this.gameState = GameState.MAIN_MENU;
    this.guessedWord = "";
    
  }
  @Override
  public GridDimension getDimension() {
    int rows = wordleBoard.rows();
    int cols = wordleBoard.cols();
    return new Grid<>(rows, cols);
    
  }
  /** 
  * Sets this model to be current view
  * @param view the view that is shown
  */
  public void setView(WordleView view) {
    this.view = view;
  }
  
  
  // Retrieves a new tetromino and places it at the top center of the board.
  public void getNewWord(){
    this.wordFactory.createWord();
  }
  
  
  @Override
  public GameState getGameState() {
    return gameState;}

  
  /** Sets the board as empty, and starts a new game */
  public void startGame() {
    score = 0;
    currentWord = wordFactory.createWord();
    gameState = GameState.ACTIVE_GAME;
    for (int i = 0; i < wordleBoard.rows(); i++) {
      
    }}


  /** Sets GameState as MainMenu */
  public void mainMenu(){
    gameState = GameState.MAIN_MENU;
    }


@Override
public boolean moveBox(int deltaRow, int deltaCol) {
	// TODO Auto-generated method stub
	throw new UnsupportedOperationException("Unimplemented method 'moveBox'");
}

}
