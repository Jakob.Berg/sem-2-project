package no.uib.inf101.wordle.model.word;

import java.util.Random;

public class RandomWordFactory implements WordFactory {
    
    private String[] validWords = {"papir, biler"};
    private Random random = new Random();

    @Override
    public Word createWord() {
        int index = random.nextInt(validWords.length);
        return new Word(validWords[index]);
    }
}
