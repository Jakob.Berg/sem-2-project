package no.uib.inf101.wordle.model.word;


public class Word {
  private String word;

  public Word(String word) {
    this.word = word;
}
public String getWord() {
  return word;
}
public void setWord(String word) {
  this.word = word;
}
public boolean isCharacterInWord(char ch) {
  for (int i = 0; i < word.length(); i++) {
      if (word.charAt(i) == ch) {
          return true;
      }
  }
  return false;
}
public boolean[] checkCorrectPositions(String guess) {
  // Ensure that the guess is the same length as the target word
  if (guess.length() != word.length()) {
      throw new IllegalArgumentException("Guess must be the same length as the target word.");
  }

  // Initialize an array to hold the correctness of each position
  boolean[] correctPositions = new boolean[word.length()];

  // Iterate over each character in the guess and compare with the target word
  for (int i = 0; i < word.length(); i++) {
      correctPositions[i] = guess.charAt(i) == word.charAt(i);
  }

  return correctPositions;
}



  
}
