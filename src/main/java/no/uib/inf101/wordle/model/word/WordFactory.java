package no.uib.inf101.wordle.model.word;

public interface WordFactory {
    /**
  * Creates a new random tetromino
  *
  * @return a new {@link tetromino} object
  */
  public Word createWord();
}
