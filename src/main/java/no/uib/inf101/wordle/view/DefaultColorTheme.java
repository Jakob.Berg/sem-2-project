package no.uib.inf101.wordle.view;

import java.awt.Color;

public class DefaultColorTheme implements ColorTheme {
    @Override
    public Color getCellColor(String theCase) {
      
      if (theCase.equals("inWord")){ 
        return Color.YELLOW;}
      else if (theCase.equals("inWordAndCorrectPlacement")){
        return Color.GREEN;}
      else{
        return Color.BLACK;
      }
      }
    
  
    
    @Override
    public Color getFrameColor() {
      return Color.LIGHT_GRAY;
    }
  
    
    @Override
    public Color getBackgroundColor() {
      return Color.BLACK;
    }
  
    @Override
    public Color getLetterColor() {
      return Color.WHITE;
    }


}
